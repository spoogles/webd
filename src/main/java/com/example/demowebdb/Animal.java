package com.example.demowebdb;

import lombok.Value;


@Value
public class Animal {
    String id;
    String name;
    String binominalName;
    String description;
    String ConservationStatus;

    public String getName() {
        return name;
    }
}
