package com.example.demowebdb;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/animals")
public class AnimalController {
    @GetMapping
    public List<Animal> all(){
        return List.of(new Animal(UUID.randomUUID().toString(),"Zebra", "Zebrus gigantus", "", ""),
                       new Animal(UUID.randomUUID().toString(),"Crocodile", "Toothus havingus maximus","",""),
                       new Animal(UUID.randomUUID().toString(),"Elephant", "Biggus Dickus","",""),
                       new Animal(UUID.randomUUID().toString(),"Cat", "Assholius Maximus","",""),
                       new Animal(UUID.randomUUID().toString(),"Dog", "Goodest boy","",""));
    }
    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal){
        return new Animal(
                UUID.randomUUID().toString(),
                createAnimal.getName(),
                createAnimal.getBinomialName(),
                "",
                ""
                );

    }
}
