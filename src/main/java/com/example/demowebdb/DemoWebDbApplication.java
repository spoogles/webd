package com.example.demowebdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWebDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWebDbApplication.class, args);
    }

}
