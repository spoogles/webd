package com.example.demowebdb;


public class CreateAnimal {
    String name;
    String binomialName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBinomialName() {
        return binomialName;
    }

    public void setBinomialName(String binomialName) {
        BinomialName = binomialName;
    }

    String BinomialName;
}
